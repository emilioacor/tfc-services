/**
 * 
 */
package org.eac.amazing.filters;

import org.eac.amazing.core.Constantes;
import org.eac.amazing.dtos.FiltersDaoDto;
import org.elasticsearch.index.query.BoolFilterBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.RangeFilterBuilder;
import org.elasticsearch.index.query.TermsFilterBuilder;
import org.springframework.stereotype.Service;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */
@Service
public class ArticleFilterBuilderImpl implements ArticleFilterBuilder {

	/**
	 * @return
	 */
	public FilterBuilder buildFilters(FiltersDaoDto filtersDaoDto) {
		BoolFilterBuilder boolFilter = new BoolFilterBuilder();
		addCategoryFilter(filtersDaoDto, boolFilter);
		addAmountFilter(filtersDaoDto, boolFilter);
		addDateFilter(filtersDaoDto, boolFilter);
		addPopularFilter(filtersDaoDto, boolFilter);
		addBestSoldFilter(filtersDaoDto, boolFilter);
		addDiscountFilter(filtersDaoDto, boolFilter);
		addRecentlyAddedFilter(filtersDaoDto, boolFilter);
		addSexFilter(filtersDaoDto, boolFilter);
		addSizeFilter(filtersDaoDto, boolFilter);
		addFormatFilter(filtersDaoDto, boolFilter);
		addColourFilter(filtersDaoDto, boolFilter);
		addAlbumGenreFilter(filtersDaoDto, boolFilter);
		addAlbumYearFilter(filtersDaoDto, boolFilter);
		addMovieGenreFilter(filtersDaoDto, boolFilter);
		addMovieYearFilter(filtersDaoDto, boolFilter);

		return boolFilter;
	}

	private void addDiscountFilter(FiltersDaoDto filtersDaoDto, BoolFilterBuilder boolFilter) {
		if (filtersDaoDto.getDiscountFilter() != null) {
			boolFilter.must(buildTermsFilter(Constantes.DISCOUNT_FIELD, filtersDaoDto.getDiscountFilter().toString()));
		}
	}

	private void addBestSoldFilter(FiltersDaoDto filtersDaoDto, BoolFilterBuilder boolFilter) {
		if (filtersDaoDto.getBestSoldFilter() != null) {
			boolFilter.must(buildTermsFilter(Constantes.BEST_SOLD_FIELD, filtersDaoDto.getBestSoldFilter().toString()));
		}
	}

	private void addMovieYearFilter(FiltersDaoDto filtersDaoDto, BoolFilterBuilder boolFilter) {
		if (filtersDaoDto.getMovieYearFilter() != null) {
			boolFilter.must(buildTermsFilter(Constantes.MOVIE_YEAR_FIELD, filtersDaoDto.getMovieYearFilter()));
		}
	}

	private void addMovieGenreFilter(FiltersDaoDto filtersDaoDto, BoolFilterBuilder boolFilter) {
		if (filtersDaoDto.getMovieGenreFilter() != null) {
			boolFilter.must(buildTermsFilter(Constantes.MOVIE_GENRE_FIELD, filtersDaoDto.getMovieGenreFilter()));
		}

	}

	private void addAlbumYearFilter(FiltersDaoDto filtersDaoDto, BoolFilterBuilder boolFilter) {
		if (filtersDaoDto.getAlbumYearFilter() != null) {
			boolFilter.must(buildTermsFilter(Constantes.ALBUM_YEAR_FIELD, filtersDaoDto.getAlbumYearFilter()));
		}

	}

	private void addAlbumGenreFilter(FiltersDaoDto filtersDaoDto, BoolFilterBuilder boolFilter) {
		if (filtersDaoDto.getAlbumGenreFilter() != null) {
			boolFilter.must(buildTermsFilter(Constantes.ALBUM_GENRE_FIELD, filtersDaoDto.getAlbumGenreFilter()));
		}
	}

	private void addColourFilter(FiltersDaoDto filtersDaoDto, BoolFilterBuilder boolFilter) {
		if (filtersDaoDto.getColourFilter() != null) {
			boolFilter.must(buildTermsFilter(Constantes.COLOUR_FIELD, filtersDaoDto.getColourFilter()));
		}

	}

	private void addFormatFilter(FiltersDaoDto filtersDaoDto, BoolFilterBuilder boolFilter) {
		if (filtersDaoDto.getFormatFilter() != null) {
			boolFilter.must(buildTermsFilter(Constantes.FORMAT_FIELD, filtersDaoDto.getFormatFilter()));
		}

	}

	private void addSizeFilter(FiltersDaoDto filtersDaoDto, BoolFilterBuilder boolFilter) {
		if (filtersDaoDto.getSizeFilter() != null) {
			boolFilter.must(buildTermsFilter(Constantes.SIZE_FIELD, filtersDaoDto.getSizeFilter()));
		}

	}

	private void addSexFilter(FiltersDaoDto filtersDaoDto, BoolFilterBuilder boolFilter) {
		if (filtersDaoDto.getSexFilter() != null) {
			boolFilter.must(buildTermsFilter(Constantes.SEX_FIELD, filtersDaoDto.getSexFilter()));
		}

	}

	/**
	 * @param filtersDaoDto
	 * @param boolFilter
	 */
	private void addRecentlyAddedFilter(FiltersDaoDto filtersDaoDto, BoolFilterBuilder boolFilter) {
		if (filtersDaoDto.getRecentlyAddedFilter() != null) {
			boolFilter.must(buildTermsFilter(Constantes.RECENTLY_ADDED_FIELD, filtersDaoDto.getRecentlyAddedFilter()
					.toString()));
		}
	}

	/**
	 * @param boolFilter
	 * @param filtersDaoDto
	 * 
	 */
	private void addPopularFilter(FiltersDaoDto filtersDaoDto, BoolFilterBuilder boolFilter) {
		if (filtersDaoDto.getPopularFilter() != null) {
			boolFilter.must(buildTermsFilter(Constantes.POPULAR_FIELD, filtersDaoDto.getPopularFilter().toString()));
		}

	}

	/**
	 * @param filtersDaoDto
	 * @param boolFilter
	 */
	private void addDateFilter(FiltersDaoDto filtersDaoDto, BoolFilterBuilder boolFilter) {
		if (filtersDaoDto.getDateFilter() != null) {
			boolFilter.must(buildRangeFilter(Constantes.DATE_FIELD, filtersDaoDto.getDateFilter()));
		}
	}

	/**
	 * @param filtersDaoDto
	 * @param boolFilter
	 */
	private void addAmountFilter(FiltersDaoDto filtersDaoDto, BoolFilterBuilder boolFilter) {
		if (filtersDaoDto.getAmountFilter() != null) {
			boolFilter.must(buildRangeFilter(Constantes.AMOUNT_FIELD, filtersDaoDto.getAmountFilter()));
		}
	}

	/**
	 * @param filtersDaoDto
	 * @param boolFilter
	 */
	private void addCategoryFilter(FiltersDaoDto filtersDaoDto, BoolFilterBuilder boolFilter) {
		if (filtersDaoDto.getCategoryFilter() != null) {
			boolFilter.must(buildTermsFilter(Constantes.CATEGORY_CODE_FIELD, filtersDaoDto.getCategoryFilter()));
		}
	}

	/**
	 * @param field
	 * @param filter
	 * @return
	 */
	private <T> FilterBuilder buildRangeFilter(String field, GenericRangeFilter<T> filter) {
		return new RangeFilterBuilder(field).from(filter.getFrom()).to(filter.getTo()).includeLower(Boolean.TRUE)
				.includeUpper(Boolean.TRUE);
	}

	/**
	 * @param field
	 * @param values
	 * @return
	 */
	private TermsFilterBuilder buildTermsFilter(String field, String... values) {
		return new TermsFilterBuilder(field, values);
	}
}
