/**
 * 
 */
package org.eac.amazing.filters;

import java.io.Serializable;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */
public class GenericRangeFilter<T> implements Serializable {

	private static final long serialVersionUID = 5160010013021410105L;

	private T from;

	private T to;

	public T getFrom() {
		return from;
	}

	public void setFrom(T from) {
		this.from = from;
	}

	public T getTo() {
		return to;
	}

	public void setTo(T to) {
		this.to = to;
	}

}
