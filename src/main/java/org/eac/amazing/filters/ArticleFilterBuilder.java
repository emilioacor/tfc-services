/**
 * 
 */
package org.eac.amazing.filters;

import org.eac.amazing.dtos.FiltersDaoDto;
import org.elasticsearch.index.query.FilterBuilder;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */
public interface ArticleFilterBuilder {

	public FilterBuilder buildFilters(FiltersDaoDto filtersDaoDto);

}
