package org.eac.amazing.remoting;

import java.util.ArrayList;

import org.eac.amazing.core.commons.enums.OrderFieldEnum;
import org.eac.amazing.core.commons.enums.OrderTypeEnum;
import org.eac.amazing.dtos.Article;
import org.eac.amazing.dtos.ArticlesSearchInDto;
import org.eac.amazing.dtos.ArticlesSearchOutDto;
import org.eac.amazing.dtos.ArticlesSearchServiceInDto;
import org.eac.amazing.dtos.ArticlesSearchServiceOutDto;
import org.eac.amazing.dtos.FacetedInfoDto;
import org.eac.amazing.dtos.FacetedInfoServiceDto;
import org.eac.amazing.dtos.FiltersDto;
import org.eac.amazing.dtos.FiltersServiceDto;
import org.eac.amazing.services.ArticlesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("articles")
public class ArticlesOperationsImpl implements ArticlesOperations {

	@Autowired
	private ArticlesService articlesService;

	@Override
	@RequestMapping(value = "search", method = RequestMethod.POST)
	@ResponseBody
	public ArticlesSearchOutDto search(@RequestBody ArticlesSearchInDto inDto) {
		ArticlesSearchServiceInDto serviceInDto = buildServiceInDto(inDto);

		return processOut(articlesService.search(serviceInDto));
	}

	private ArticlesSearchOutDto processOut(ArticlesSearchServiceOutDto search) {
		ArticlesSearchOutDto outDto = new ArticlesSearchOutDto();
		outDto.setArticles(new ArrayList<Article>(search.getArticles()));
		outDto.setFacetedInfoDto(buildFacetedInfo(search.getFacetedInfoDto()));
		outDto.setTotal(search.getTotal());

		return outDto;
	}

	/**
	 * @param facetedInfoDto
	 * @return
	 */
	private FacetedInfoDto buildFacetedInfo(FacetedInfoServiceDto source) {
		FacetedInfoDto target = new FacetedInfoDto();
		target.setAmountFacetInfo(source.getAmountFacetInfo());
		target.setCategoryFacetInfo(source.getCategoryFacetInfo());
		target.setPopularFacetInfo(source.getPopularFacetInfo());
		target.setRecentlyAddedFacetInfo(source.getRecentlyAddedFacetInfo());
		target.setSizeFacetInfo(source.getSizeFacetInfo());
		target.setSexFacetInfo(source.getSexFacetInfo());
		target.setAlbumYearFacetInfo(source.getAlbumYearFacetInfo());
		target.setMovieYearFacetInfo(source.getMovieYearFacetInfo());
		target.setColourFacetInfo(source.getColourFacetInfo());
		target.setFormatFacetInfo(source.getFormatFacetInfo());
		target.setAlbumGenreFacetInfo(source.getAlbumGenreFacetInfo());
		target.setMovieGenreFacetInfo(source.getMovieGenreFacetInfo());

		return target;
	}

	private ArticlesSearchServiceInDto buildServiceInDto(ArticlesSearchInDto inDto) {
		ArticlesSearchServiceInDto serviceDto = new ArticlesSearchServiceInDto();
		serviceDto.setSearch(inDto.getSearch());
		serviceDto.setFilters(buildServiceFilters(inDto.getFilters()));
		serviceDto.setOrderBy(buildOrderBy(inDto));
		serviceDto.setOrderType(buildOrderType(inDto));
		serviceDto.setFrom(inDto.getFrom());

		return serviceDto;
	}

	private OrderTypeEnum buildOrderType(ArticlesSearchInDto inDto) {
		if (inDto.getOrderBy() != null) {
			return inDto.getOrderType();

		} else {
			return OrderTypeEnum.DESC;
		}
	}

	private OrderFieldEnum buildOrderBy(ArticlesSearchInDto inDto) {
		if (inDto.getOrderBy() != null) {
			return inDto.getOrderBy();
		} else {
			return OrderFieldEnum.POPULAR;
		}
	}

	/**
	 * @param filters
	 * @return
	 */
	private FiltersServiceDto buildServiceFilters(FiltersDto filters) {
		FiltersServiceDto serviceFilters = null;

		if (filters != null) {
			serviceFilters = new FiltersServiceDto();
			serviceFilters.setAmountFilter(filters.getAmountFilter());
			serviceFilters.setCategoryFilter(filters.getCategoryFilter());
			serviceFilters.setDateFilter(filters.getDateFilter());
			serviceFilters.setPopularFilter(filters.getPopularFilter());
			serviceFilters.setRecentlyAddedFilter(filters.getRecentlyAddedFilter());
			serviceFilters.setSexFilter(filters.getSexFilter());
			serviceFilters.setSizeFilter(filters.getSizeFilter());
			serviceFilters.setColourFilter(filters.getColourFilter());
			serviceFilters.setFormatFilter(filters.getFormatFilter());
			serviceFilters.setAlbumGenreFilter(filters.getAlbumGenreFilter());
			serviceFilters.setAlbumYearFilter(filters.getAlbumYearFilter());
			serviceFilters.setMovieGenreFilter(filters.getMovieGenreFilter());
			serviceFilters.setMovieYearFilter(filters.getMovieYearFilter());
			serviceFilters.setBestSoldFilter(filters.getBestSoldFilter());
			serviceFilters.setDiscountFilter(filters.getDiscountFilter());
		}

		return serviceFilters;
	}

}
