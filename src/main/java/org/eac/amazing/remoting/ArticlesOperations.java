package org.eac.amazing.remoting;

import org.eac.amazing.dtos.ArticlesSearchInDto;
import org.eac.amazing.dtos.ArticlesSearchOutDto;

public interface ArticlesOperations {

	public ArticlesSearchOutDto search(ArticlesSearchInDto inDto);

}