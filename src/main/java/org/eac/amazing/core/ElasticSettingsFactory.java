package org.eac.amazing.core;

import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.springframework.beans.factory.FactoryBean;

public class ElasticSettingsFactory implements FactoryBean<Settings> {

	@Override
	public Settings getObject() throws Exception {
		return ImmutableSettings.settingsBuilder().put("cluster.name", "amazing_cluster").build();
	}

	@Override
	public Class<?> getObjectType() {
		return Settings.class;
	}

	@Override
	public boolean isSingleton() {
		return Boolean.TRUE;
	}

}
