package org.eac.amazing.core;

import io.searchbox.client.config.ClientConfig;

import java.util.Arrays;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;

public class ClientConfigFactory implements FactoryBean<ClientConfig>, InitializingBean {

	private static final String DEFAULT_SERVER = "http://localhost:9200";

	@Value(value = "${server}")
	private String[] server;

	@Override
	public ClientConfig getObject() throws Exception {
		ClientConfig clientConfig = new ClientConfig.Builder(Arrays.asList(server)).build();

		return clientConfig;
	}

	@Override
	public Class<?> getObjectType() {
		return ClientConfig.class;
	}

	@Override
	public boolean isSingleton() {
		return Boolean.TRUE;
	}

	@Override
	public void afterPropertiesSet() throws Exception {

		if (server == null || server.length == 0) {
			server = new String[] { DEFAULT_SERVER };
		}
	}
}
