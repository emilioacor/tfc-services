/**
 * 
 */
package org.eac.amazing.core;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */
public class Constantes {

	public static final String CATEGORY_CODE_FIELD = "categoryCode";
	public static final String AMOUNT_FIELD = "amount";
	public static final String DATE_FIELD = "date";
	public static final String POPULAR_FIELD = "popular";
	public static final String RECENTLY_ADDED_FIELD = "recentlyAdded";
	public static final String MOVIE_YEAR_FIELD = "movieYear";
	public static final String ALBUM_YEAR_FIELD = "albumYear";
	public static final String MOVIE_GENRE_FIELD = "movieGenre";
	public static final String ALBUM_GENRE_FIELD = "albumGenre";
	public static final String SIZE_FIELD = "size";
	public static final String SEX_FIELD = "sex";
	public static final String COLOUR_FIELD = "colour";
	public static final String FORMAT_FIELD = "format";
	public static final String DISCOUNT_FIELD = "discount";
	public static final String BEST_SOLD_FIELD = "bestSold";

}
