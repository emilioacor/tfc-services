package org.eac.amazing.core;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Value;

public class ElasticClientFactory implements FactoryBean<TransportClient> {

	// @Autowired
	// @Qualifier("elasticSettings")
	// private Settings elasticSettings;

	@Value(value = "${server}")
	private String server;

	@Value(value = "${port}")
	private Integer port;

	@Override
	public TransportClient getObject() throws Exception {
		Settings elasticSettings = ImmutableSettings.settingsBuilder().put("cluster.name", "amazing_cluster").build();
		return new TransportClient(elasticSettings).addTransportAddress(new InetSocketTransportAddress(server, port));
	}

	@Override
	public Class<?> getObjectType() {
		return TransportClient.class;
	}

	@Override
	public boolean isSingleton() {
		return Boolean.TRUE;

	}

}
