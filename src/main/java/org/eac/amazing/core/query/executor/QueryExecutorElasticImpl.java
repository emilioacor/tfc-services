/**
 * 
 */
package org.eac.amazing.core.query.executor;

import io.searchbox.Action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.eac.amazing.core.query.SearchQueryBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilteredQueryBuilder;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.facet.FacetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */

public class QueryExecutorElasticImpl implements QueryExecutor {

	@Autowired
	@Qualifier("elasticClient")
	private Client elasticClient;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eac.amazing.core.query.executor.QueryExecutor# executeQueryWithResultAsList(io.searchbox.Action,
	 * java.lang.Class)
	 */
	@Override
	public <T> QueryResult<T> executeQueryWithResultAsList(Action searchQuery, Class<T> documentClass) {
		return null;

	}

	@Override
	public <T> QueryResult<T> executeQueryWithResultAsList(SearchQueryBuilder searchQueryBuilder, String indexes,
			String types, Class<T> documentClass) {
		return null;

	}

	@Override
	public <T> QueryResult<T> executeQueryWithResultAsList(FilteredQueryBuilder filteredQueryBuilder,
			FilterBuilder filterBuilder, FacetBuilder[] facets, String indexes, String types, Integer from,
			Integer size, Class<T> documentClass) {
		QueryResult<T> queryResults = null;
		try {
			SearchRequestBuilder searchRequestBuilder = elasticClient.prepareSearch(indexes).setTypes(types)
					.setFilter(filterBuilder).setFrom(from).setSize(size);

			for (FacetBuilder facet : facets) {
				searchRequestBuilder = searchRequestBuilder.addFacet(facet);
			}

			SearchResponse searchResponse = searchRequestBuilder.get();
			if (RestStatus.OK.equals(searchResponse.status())) {
				queryResults = new QueryResult<T>();
				queryResults.setResults(getHitsAsList(searchResponse, documentClass));
				queryResults.setTotalElements(searchResponse.getHits().getTotalHits());
			}
			// if (jestResults.isSucceeded()) {
			// queryResults = new QueryResult<T>();
			// queryResults.setResults(jestResults.getSourceAsObjectList(documentClass));
			// queryResults.setRangeFacets(jestResults.getFacets(RangeFacet.class));
			// queryResults.setTermsFacets(jestResults.getFacets(TermsFacet.class));
			// }
		} catch (Exception e) {
			e.printStackTrace();
		}

		return queryResults;
	}

	private <T> List<T> getHitsAsList(SearchResponse searchResponse, Class<T> type) {
		List<T> list = new ArrayList<T>();
		ObjectMapper mapper = new ObjectMapper();
		for (SearchHit hit : searchResponse.getHits()) {
			try {
				T object = mapper.readValue(hit.getSourceAsString().getBytes(), type);
				list.add(object);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		;
		return list;
	}
}
