package org.eac.amazing.core.query.executor;

import io.searchbox.core.search.facet.RangeFacet;
import io.searchbox.core.search.facet.TermsFacet;

import java.io.Serializable;
import java.util.List;

/**
 * @author Emilio Ambrosio Cordero
 * 
 * @param <T>
 */
public class QueryResult<T> implements Serializable {

	private static final long serialVersionUID = -9103417286257848812L;

	private List<T> results;

	private List<RangeFacet> rangeFacets;

	private List<TermsFacet> termsFacets;

	private Long totalElements;

	private Long size;

	public List<T> getResults() {
		return results;
	}

	public void setResults(List<T> results) {
		this.results = results;
	}

	public Long getTotalElements() {
		return totalElements;
	}

	public void setTotalElements(Long totalElements) {
		this.totalElements = totalElements;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public List<RangeFacet> getRangeFacets() {
		return rangeFacets;
	}

	public void setRangeFacets(List<RangeFacet> rangeFacets) {
		this.rangeFacets = rangeFacets;
	}

	public List<TermsFacet> getTermsFacets() {
		return termsFacets;
	}

	public void setTermsFacets(List<TermsFacet> termsFacets) {
		this.termsFacets = termsFacets;
	}

}
