package org.eac.amazing.core.query;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.facet.FacetBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.util.CollectionUtils;

public class SearchQueryBuilder extends SearchRequestBuilder implements Serializable {

	private static final long serialVersionUID = 39425552398356579L;

	private SearchQueryBuilder() {
		super(null);
	}

	public SearchQueryBuilder(QueryBuilder queryBuilder) {
		this();
		super.setQuery(queryBuilder);
	}

	public SearchQueryBuilder withFilters(FilterBuilder filter) {
		super.setFilter(filter);
		return this;
	}

	public SearchQueryBuilder withFacets(FacetBuilder... facets) {
		return withFacets(Arrays.asList(facets));
	}

	public SearchQueryBuilder withFacets(List<FacetBuilder> facets) {
		if (!CollectionUtils.isEmpty(facets)) {
			for (FacetBuilder facet : facets) {
				super.addFacet(facet);
			}
		}

		return this;
	}

	public SearchQueryBuilder sortedBy(String sortField) {
		sortedBy(sortField, SortOrder.DESC);
		return this;
	}

	/**
	 * @param sortField
	 * @param sortOrder
	 */
	public SearchQueryBuilder sortedBy(String sortField, SortOrder sortOrder) {
		super.addSort(sortField, sortOrder);
		return this;
	}

	public SearchQueryBuilder onIndexes(String... indexes) {
		super.setIndices(indexes);
		return this;

	}

	public SearchQueryBuilder forTypes(String... types) {
		super.setTypes(types);
		return this;

	}

	public SearchQueryBuilder withRoutings(String... routings) {
		super.setRouting(routings);
		return this;

	}

	public SearchQueryBuilder withRoutings(List<String> routings) {
		if (!CollectionUtils.isEmpty(routings)) {
			withRoutings(routings);
		}

		return this;

	}

	public SearchQueryBuilder withSize(int size) {
		super.setSize(size);
		return this;

	}

	public SearchQueryBuilder from(int from) {
		super.setFrom(from);
		return this;

	}
}
