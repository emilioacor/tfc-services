package org.eac.amazing.core.query.executor;

import io.searchbox.Action;

import org.eac.amazing.core.query.SearchQueryBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilteredQueryBuilder;
import org.elasticsearch.search.facet.FacetBuilder;

public interface QueryExecutor {

	public <T> QueryResult<T> executeQueryWithResultAsList(Action searchQuery, Class<T> documentClass);

	public <T> QueryResult<T> executeQueryWithResultAsList(SearchQueryBuilder searchQueryBuilder, String indexes,
			String types, Class<T> documentClass);

	public <T> QueryResult<T> executeQueryWithResultAsList(FilteredQueryBuilder filteredQueryBuilder,
			FilterBuilder filterBuilder, FacetBuilder[] facets, String indexes, String types, Integer from,
			Integer size, Class<T> documentClass);

}
