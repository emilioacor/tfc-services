/**
 * 
 */
package org.eac.amazing.core.query.executor;

import io.searchbox.Action;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.search.facet.RangeFacet;
import io.searchbox.core.search.facet.TermsFacet;

import org.eac.amazing.core.query.SearchQueryBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilteredQueryBuilder;
import org.elasticsearch.search.facet.FacetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */
@Service
public class QueryExecutorImpl implements QueryExecutor {

	@Autowired
	@Qualifier("jestClient")
	private JestClient jestClient;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eac.amazing.core.query.executor.QueryExecutor# executeQueryWithResultAsList(io.searchbox.Action,
	 * java.lang.Class)
	 */
	@Override
	public <T> QueryResult<T> executeQueryWithResultAsList(Action searchQuery, Class<T> documentClass) {

		QueryResult<T> queryResults = null;
		try {
			JestResult jestResults = jestClient.execute(searchQuery);
			if (jestResults.isSucceeded()) {
				queryResults = new QueryResult<T>();
				queryResults.setResults(jestResults.getSourceAsObjectList(documentClass));
				queryResults.setRangeFacets(jestResults.getFacets(RangeFacet.class));
				queryResults.setTermsFacets(jestResults.getFacets(TermsFacet.class));
				JsonObject jsonHits = (JsonObject) jestResults.getJsonObject().get("hits");
				queryResults.setTotalElements(jsonHits.get("total").getAsLong());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return queryResults;
	}

	@Override
	public <T> QueryResult<T> executeQueryWithResultAsList(SearchQueryBuilder searchQueryBuilder, String indexes,
			String types, Class<T> documentClass) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eac.amazing.core.query.executor.QueryExecutor#executeQueryWithResultAsList(org.elasticsearch.index.query.
	 * FilteredQueryBuilder, org.elasticsearch.index.query.FilterBuilder, org.elasticsearch.search.facet.FacetBuilder[],
	 * java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer, java.lang.Class)
	 */
	@Override
	public <T> QueryResult<T> executeQueryWithResultAsList(FilteredQueryBuilder filteredQueryBuilder,
			FilterBuilder filterBuilder, FacetBuilder[] facets, String indexes, String types, Integer from,
			Integer size, Class<T> documentClass) {
		// TODO Auto-generated method stub
		return null;
	}

}
