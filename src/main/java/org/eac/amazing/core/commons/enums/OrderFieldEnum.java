package org.eac.amazing.core.commons.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonFormat(shape = Shape.STRING)
public enum OrderFieldEnum {
	AMOUNT("amount"), POPULAR("popular");

	private String field;

	private OrderFieldEnum(String field) {
		this.field = field;
	}

	@JsonValue
	public String getField() {
		return field;
	}

	@JsonCreator
	public static OrderFieldEnum create(String field) {
		OrderFieldEnum[] fields = OrderFieldEnum.values();
		for (OrderFieldEnum orderFieldEnum : fields) {
			if (orderFieldEnum.getField().equalsIgnoreCase(field)) {
				return orderFieldEnum;
			}
		}

		return null;

	}

}
