package org.eac.amazing.core.commons.enums;

public enum FacetEnum {
	CATEGORY,
	AMOUNT,
	SIZE,
	POPULAR,
	RECENTLY_ADDED,
	SEX,
	COLOUR,
	FORMAT,
	ALBUM_GENRE,
	ALBUM_YEAR,
	MOVIE_GENRE,
	MOVIE_YEAR,
	BEST_SOLD,
	DISCOUNT

}
