/**
 * 
 */
package org.eac.amazing.core.commons.configuration;

import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */
@Service
public class ConfiguratorImpl implements Configurator {

	@Autowired
	private Properties properties;

	@Value(value = "${multiMatchFields}")
	private String multiMatchFields;

	@Value(value = "${typeName}")
	private String typeName;

	@Value(value = "${indexName}")
	private String indexName;

	@Value(value = "${categories}")
	private String categories;

	@Value(value = "${size}")
	private String size;

	private static final String COMMA_SEPARATOR = ",";

	@Override
	public String getIndexes() {
		return indexName;
	}

	@Override
	public String getTypes() {
		return typeName;
	}

	@Override
	public String[] getMultiMatchFields() {
		return getPropertyAsArray(multiMatchFields);
	}

	@Override
	public String[] getCategories() {
		return getPropertyAsArray(categories);
	}

	/**
	 * @param indexname2
	 * @return
	 */
	private String getProperty(String property) {
		return properties.getProperty(property, StringUtils.EMPTY);
	}

	/**
	 * @param property
	 * @return
	 */
	private String[] getPropertyAsArray(String property) {
		String value = property;
		return StringUtils.split(value, COMMA_SEPARATOR);
	}

	private Integer getPropertyAsInteger(String property) {
		return Integer.valueOf(properties.getProperty(property, "10"));
	}

	@Override
	public Integer getSize() {
		return getPropertyAsInteger(size);
	}
}
