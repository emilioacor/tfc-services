/**
 * 
 */
package org.eac.amazing.core.commons.configuration;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */
public interface Configurator {

	String getIndexes();

	String getTypes();

	String[] getMultiMatchFields();

	String[] getCategories();

	Integer getSize();

}
