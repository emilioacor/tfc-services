/**
 * 
 */
package org.eac.amazing.core;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.ClientConfig;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */
public class ClientFactory extends JestClientFactory implements FactoryBean<JestClient>, InitializingBean {

	@Autowired
	@Qualifier("clientConfig")
	private ClientConfig clientConfig;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public JestClient getObject() {
		super.setClientConfig(clientConfig);
		JestClient client = super.getObject();

		return client;
	}

}
