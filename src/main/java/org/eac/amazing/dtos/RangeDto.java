/**
 * 
 */
package org.eac.amazing.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RangeDto<T> {

	private T from;
	private T to;
	private Long totalCount;

}
