package org.eac.amazing.dtos;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.eac.amazing.core.commons.enums.OrderFieldEnum;
import org.eac.amazing.core.commons.enums.OrderTypeEnum;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticlesSearchInDto implements Serializable {

	private static final long serialVersionUID = 4752294963871438210L;

	private String search;
	private FiltersDto filters;
	private OrderFieldEnum orderBy;
	private OrderTypeEnum orderType;
	private Integer from;

}
