package org.eac.amazing.dtos;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Article implements Serializable {

	private static final long serialVersionUID = 8485552716038570403L;

	private String name;
	private Double amount;
	private String categoryCode;
	private String categoryDescription;
	private String articleCode;
	private String articleDescription;
	private String tradeMark;
	private Boolean recentlyAdded;
	private Boolean popular;
	private Boolean bestSold;
	private Boolean discount;
	private Double lastAmount;
	private String director;
	private String movieGenre;
	private Integer movieYear;
	private String format;
	private String colour;
	private String size;
	private String sex;
	private String album;
	private String author;
	private String albumGenre;
	private String albumYear;

}
