package org.eac.amazing.dtos;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.eac.amazing.core.commons.enums.OrderFieldEnum;
import org.eac.amazing.core.commons.enums.OrderTypeEnum;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticlesSearchServiceInDto {

	@NotNull
	private String search;
	private FiltersServiceDto filters;
	private OrderFieldEnum orderBy;
	private OrderTypeEnum orderType;
	private Integer from;

}
