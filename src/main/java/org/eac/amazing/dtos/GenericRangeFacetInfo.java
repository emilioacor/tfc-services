/**
 * 
 */
package org.eac.amazing.dtos;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenericRangeFacetInfo<T> implements Serializable {

	private static final long serialVersionUID = 2837860682127181122L;

	private String name;
	private List<RangeDto<T>> ranges;

}
