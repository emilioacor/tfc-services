package org.eac.amazing.dtos;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticlesSearchOutDto implements Serializable {

	private static final long serialVersionUID = 4343389617118657334L;

	private List<Article> articles;
	private FacetedInfoDto facetedInfoDto;
	private long total;

}
