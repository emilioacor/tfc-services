package org.eac.amazing.dtos;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticlesSearchServiceOutDto {

	private List<Article> articles;
	private FacetedInfoServiceDto facetedInfoDto;
	private long total;

}
