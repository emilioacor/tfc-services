package org.eac.amazing.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.eac.amazing.core.commons.enums.OrderFieldEnum;
import org.elasticsearch.search.sort.SortOrder;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticlesSearchDaoInDto {

	private String search;
	private FiltersDaoDto filters;
	private OrderFieldEnum orderBy;
	private SortOrder orderType;
	private Integer from;

}
