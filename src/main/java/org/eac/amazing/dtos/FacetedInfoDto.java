/**
 * 
 */
package org.eac.amazing.dtos;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FacetedInfoDto implements Serializable {

	private static final long serialVersionUID = -845300110744297178L;

	private GenericRangeFacetInfo<Double> amountFacetInfo;
	private TermFacetInfo categoryFacetInfo;
	private TermFacetInfo sizeFacetInfo;
	private TermFacetInfo popularFacetInfo;
	private TermFacetInfo recentlyAddedFacetInfo;
	private TermFacetInfo sexFacetInfo;
	private TermFacetInfo colourFacetInfo;
	private TermFacetInfo albumGenreFacetInfo;
	private TermFacetInfo movieGenreFacetInfo;
	private TermFacetInfo albumYearFacetInfo;
	private TermFacetInfo movieYearFacetInfo;
	private TermFacetInfo formatFacetInfo;
	private TermFacetInfo bestSoldFacetInfo;
	private TermFacetInfo discountFacetInfo;
}
