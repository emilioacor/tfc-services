/**
 * 
 */
package org.eac.amazing.dtos;

import io.searchbox.core.search.facet.TermsFacet.Term;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TermFacetInfo implements Serializable {

	private static final long serialVersionUID = 4718759306051615465L;

	private String name;
	private List<Term> terms;

}
