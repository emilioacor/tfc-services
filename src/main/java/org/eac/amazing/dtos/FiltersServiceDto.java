/**
 * 
 */
package org.eac.amazing.dtos;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.eac.amazing.filters.GenericRangeFilter;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FiltersServiceDto {

	private GenericRangeFilter<Long> amountFilter;
	private GenericRangeFilter<Date> dateFilter;
	private String[] categoryFilter;
	private Boolean popularFilter;
	private Boolean recentlyAddedFilter;
	private String[] sizeFilter;
	private String[] colourFilter;
	private String[] sexFilter;
	private String[] formatFilter;
	private String[] movieGenreFilter;
	private String[] movieYearFilter;
	private String[] albumGenreFilter;
	private String[] albumYearFilter;
	private Boolean bestSoldFilter;
	private Boolean discountFilter;

}
