/**
 * 
 */
package org.eac.amazing.dtos;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.eac.amazing.filters.GenericRangeFilter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */
@JsonInclude(Include.NON_NULL)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FiltersDto implements Serializable {

	private static final long serialVersionUID = -5802770811335614613L;
	private GenericRangeFilter<Long> amountFilter;
	private GenericRangeFilter<Date> dateFilter;
	private String[] categoryFilter;
	private Boolean popularFilter;
	private Boolean recentlyAddedFilter;
	private String[] sizeFilter;
	private String[] colourFilter;
	private String[] sexFilter;
	private String[] formatFilter;
	private String[] movieGenreFilter;
	private String[] movieYearFilter;
	private String[] albumGenreFilter;
	private String[] albumYearFilter;
	private Boolean bestSoldFilter;
	private Boolean discountFilter;

}
