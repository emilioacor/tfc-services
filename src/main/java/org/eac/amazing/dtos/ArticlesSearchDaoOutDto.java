package org.eac.amazing.dtos;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticlesSearchDaoOutDto {

	private List<Article> articles;
	private FacetedInfoDaoDto facetedInfoDto;
	private long total;

}
