/**
 * 
 */
package org.eac.amazing.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FacetedInfoDaoDto {

	private GenericRangeFacetInfo<Double> amountFacetInfo;
	private TermFacetInfo categoryFacetInfo;
	private TermFacetInfo sizeFacetInfo;
	private TermFacetInfo popularFacetInfo;
	private TermFacetInfo recentlyAddedFacetInfo;
	private TermFacetInfo sexFacetInfo;
	private TermFacetInfo colourFacetInfo;
	private TermFacetInfo movieGenreFacetInfo;
	private TermFacetInfo albumGenreFacetInfo;
	private TermFacetInfo movieYearFacetInfo;
	private TermFacetInfo albumYearFacetInfo;
	private TermFacetInfo formatFacetInfo;
	private TermFacetInfo bestSoldFacetInfo;
	private TermFacetInfo discountFacetInfo;

}
