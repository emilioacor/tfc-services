/**
 * 
 */
package org.eac.amazing.facets;

import io.searchbox.core.search.facet.RangeFacet.Range;
import io.searchbox.core.search.facet.TermsFacet;
import io.searchbox.core.search.facet.TermsFacet.Term;

import java.util.ArrayList;
import java.util.List;

import org.eac.amazing.core.commons.enums.FacetEnum;
import org.eac.amazing.dtos.FacetedInfoDaoDto;
import org.eac.amazing.dtos.GenericRangeFacetInfo;
import org.eac.amazing.dtos.RangeDto;
import org.eac.amazing.dtos.TermFacetInfo;
import org.elasticsearch.search.facet.FacetBuilder;
import org.elasticsearch.search.facet.FacetBuilders;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */
@Service
public class ArticleFacetBuilderImpl implements ArticleFacetBuilder {

	/**
	 * @param facets
	 * @return
	 */
	@Override
	public FacetedInfoDaoDto buildFacetedInfo(List<io.searchbox.core.search.facet.RangeFacet> rangeFacets,
			List<TermsFacet> termsFacets) {
		FacetedInfoDaoDto facetedInfo = new FacetedInfoDaoDto();
		if (!CollectionUtils.isEmpty(rangeFacets)) {
			facetedInfo.setAmountFacetInfo(buildAmountFacetInfo(rangeFacets));
		}

		if (!CollectionUtils.isEmpty(termsFacets)) {
			facetedInfo.setCategoryFacetInfo(buildGenericTermFacetInfo(termsFacets, FacetEnum.CATEGORY));
			facetedInfo.setPopularFacetInfo(buildGenericTermFacetInfo(termsFacets, FacetEnum.POPULAR));
			facetedInfo.setBestSoldFacetInfo(buildGenericTermFacetInfo(termsFacets, FacetEnum.BEST_SOLD));
			facetedInfo.setDiscountFacetInfo(buildGenericTermFacetInfo(termsFacets, FacetEnum.DISCOUNT));
			facetedInfo.setRecentlyAddedFacetInfo(buildGenericTermFacetInfo(termsFacets, FacetEnum.RECENTLY_ADDED));
			facetedInfo.setSizeFacetInfo(buildGenericTermFacetInfo(termsFacets, FacetEnum.SIZE));
			facetedInfo.setSexFacetInfo(buildGenericTermFacetInfo(termsFacets, FacetEnum.SEX));
			facetedInfo.setColourFacetInfo(buildGenericTermFacetInfo(termsFacets, FacetEnum.COLOUR));
			facetedInfo.setAlbumYearFacetInfo(buildGenericTermFacetInfo(termsFacets, FacetEnum.ALBUM_YEAR));
			facetedInfo.setMovieYearFacetInfo(buildGenericTermFacetInfo(termsFacets, FacetEnum.MOVIE_YEAR));
			facetedInfo.setAlbumGenreFacetInfo(buildGenericTermFacetInfo(termsFacets, FacetEnum.ALBUM_GENRE));
			facetedInfo.setMovieGenreFacetInfo(buildGenericTermFacetInfo(termsFacets, FacetEnum.MOVIE_GENRE));
			facetedInfo.setFormatFacetInfo(buildGenericTermFacetInfo(termsFacets, FacetEnum.FORMAT));
		}

		return facetedInfo;
	}

	private TermFacetInfo buildGenericTermFacetInfo(List<TermsFacet> termsFacets, FacetEnum facetEnum) {
		TermFacetInfo termFacetInfo = null;

		for (TermsFacet termFacet : termsFacets) {
			if (termFacet.getName().equalsIgnoreCase(facetEnum.name())) {
				termFacetInfo = new TermFacetInfo();
				termFacetInfo.setName(termFacet.getName());
				List<Term> terms = new ArrayList<TermsFacet.Term>();
				termFacetInfo.setTerms(terms);
				for (TermsFacet.Term term : termFacet.terms()) {
					terms.add(term);
				}
			}
		}
		return termFacetInfo;
	}

	/**
	 * @param rangeFacets
	 * @return
	 */
	private GenericRangeFacetInfo<Double> buildAmountFacetInfo(
			List<io.searchbox.core.search.facet.RangeFacet> rangeFacets) {
		GenericRangeFacetInfo<Double> amountFacetedInfo = new GenericRangeFacetInfo<Double>();
		for (io.searchbox.core.search.facet.RangeFacet rangeFacet : rangeFacets) {
			if (FacetEnum.AMOUNT.name().equalsIgnoreCase(rangeFacet.getName())) {
				amountFacetedInfo.setName(rangeFacet.getName());
				amountFacetedInfo.setRanges(buildRanges(rangeFacet.getRanges()));

			}
		}
		return amountFacetedInfo;
	}

	/**
	 * @param ranges
	 * @return
	 */
	private List<RangeDto<Double>> buildRanges(List<Range> ranges) {
		List<RangeDto<Double>> list = new ArrayList<RangeDto<Double>>();
		RangeDto<Double> rangeDto = null;
		for (Range range : ranges) {
			rangeDto = new RangeDto<Double>();
			rangeDto.setFrom(range.getFrom());
			rangeDto.setTo(range.getTo());
			rangeDto.setTotalCount(range.getTotalCount());
			list.add(rangeDto);
		}

		return list;
	}

	/**
	 * @return
	 */
	public FacetBuilder buildCategoryFacet() {
		return FacetBuilders.termsFacet(FacetEnum.CATEGORY.name()).field("categoryCode");
	}

	/**
	 * @return
	 */
	public FacetBuilder buildAmountFacet() {
		return FacetBuilders.rangeFacet(FacetEnum.AMOUNT.name()).field("amount").addRange(0.0, 10.0)
				.addRange(10.0, 100.0).addRange(100.0, 1000.0).addRange(1000.0, 10000.0);
	}

	@Override
	public FacetBuilder buildSizeFacet() {
		return FacetBuilders.termsFacet(FacetEnum.SIZE.name()).field("size");
	}

	@Override
	public FacetBuilder buildPopularFacet() {
		return FacetBuilders.termsFacet(FacetEnum.POPULAR.name()).field("popular");
	}

	@Override
	public FacetBuilder buildRecentlyFacet() {
		return FacetBuilders.termsFacet(FacetEnum.RECENTLY_ADDED.name()).field("recentlyAdded");
	}

	@Override
	public FacetBuilder buildSexFacet() {
		return FacetBuilders.termsFacet(FacetEnum.SEX.name()).field("sex");
	}

	@Override
	public FacetBuilder buildColourFacet() {
		return FacetBuilders.termsFacet(FacetEnum.COLOUR.name()).field("colour");
	}

	@Override
	public FacetBuilder buildFormatFacet() {
		return FacetBuilders.termsFacet(FacetEnum.FORMAT.name()).field("format");
	}

	@Override
	public FacetBuilder builderAlbumGenreFacet() {
		return FacetBuilders.termsFacet(FacetEnum.ALBUM_GENRE.name()).field("albumGenre");
	}

	@Override
	public FacetBuilder builderMovieGenreFacet() {
		return FacetBuilders.termsFacet(FacetEnum.MOVIE_GENRE.name()).field("movieGenre");
	}

	@Override
	public FacetBuilder buildAlbumYearFacet() {
		return FacetBuilders.termsFacet(FacetEnum.ALBUM_YEAR.name()).field("albumYear");
	}

	@Override
	public FacetBuilder buildMovieYearFacet() {
		return FacetBuilders.termsFacet(FacetEnum.MOVIE_YEAR.name()).field("movieYear");
	}

	@Override
	public FacetBuilder buildDiscountFacet() {
		return FacetBuilders.termsFacet(FacetEnum.DISCOUNT.name()).field("discount");

	}

	@Override
	public FacetBuilder buildBestSoldFacet() {
		return FacetBuilders.termsFacet(FacetEnum.BEST_SOLD.name()).field("bestSold");

	}

}
