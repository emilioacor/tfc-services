/**
 * 
 */
package org.eac.amazing.facets;

import io.searchbox.core.search.facet.RangeFacet;
import io.searchbox.core.search.facet.TermsFacet;

import java.util.List;

import org.eac.amazing.dtos.FacetedInfoDaoDto;
import org.elasticsearch.search.facet.FacetBuilder;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */
public interface ArticleFacetBuilder {

	FacetedInfoDaoDto buildFacetedInfo(List<RangeFacet> rangeFacets, List<TermsFacet> termsFacets);

	FacetBuilder buildCategoryFacet();

	FacetBuilder buildAmountFacet();

	FacetBuilder buildSizeFacet();

	FacetBuilder buildPopularFacet();

	FacetBuilder buildRecentlyFacet();

	FacetBuilder buildSexFacet();

	FacetBuilder buildColourFacet();

	FacetBuilder buildFormatFacet();

	FacetBuilder buildAlbumYearFacet();

	FacetBuilder builderAlbumGenreFacet();

	FacetBuilder builderMovieGenreFacet();

	FacetBuilder buildMovieYearFacet();

	FacetBuilder buildDiscountFacet();

	FacetBuilder buildBestSoldFacet();

}
