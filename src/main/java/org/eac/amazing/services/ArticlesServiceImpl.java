package org.eac.amazing.services;

import java.util.ArrayList;

import javax.validation.Valid;

import org.eac.amazing.core.commons.configuration.Configurator;
import org.eac.amazing.daos.ArticlesDao;
import org.eac.amazing.dtos.Article;
import org.eac.amazing.dtos.ArticlesSearchDaoInDto;
import org.eac.amazing.dtos.ArticlesSearchDaoOutDto;
import org.eac.amazing.dtos.ArticlesSearchServiceInDto;
import org.eac.amazing.dtos.ArticlesSearchServiceOutDto;
import org.eac.amazing.dtos.FacetedInfoDaoDto;
import org.eac.amazing.dtos.FacetedInfoServiceDto;
import org.eac.amazing.dtos.FiltersDaoDto;
import org.eac.amazing.dtos.FiltersServiceDto;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArticlesServiceImpl implements ArticlesService {

	@Autowired
	private ArticlesDao articlesDao;

	@Autowired
	private Configurator configurator;

	@Override
	public ArticlesSearchServiceOutDto search(@Valid ArticlesSearchServiceInDto inDto) {
		ArticlesSearchDaoInDto daoInDto = buildDaoInDto(inDto);

		return proccessOut(articlesDao.search(daoInDto));
	}

	private ArticlesSearchServiceOutDto proccessOut(ArticlesSearchDaoOutDto daoOutDto) {
		ArticlesSearchServiceOutDto serviceOutDto = new ArticlesSearchServiceOutDto();
		serviceOutDto.setArticles(new ArrayList<Article>(daoOutDto.getArticles()));
		serviceOutDto.setFacetedInfoDto(buildFacetedInfo(daoOutDto.getFacetedInfoDto()));
		serviceOutDto.setTotal(daoOutDto.getTotal());

		return serviceOutDto;
	}

	/**
	 * @param facetedInfoDto
	 * @return
	 */
	private FacetedInfoServiceDto buildFacetedInfo(FacetedInfoDaoDto source) {
		FacetedInfoServiceDto target = new FacetedInfoServiceDto();
		target.setAmountFacetInfo(source.getAmountFacetInfo());
		target.setCategoryFacetInfo(source.getCategoryFacetInfo());
		target.setPopularFacetInfo(source.getPopularFacetInfo());
		target.setRecentlyAddedFacetInfo(source.getRecentlyAddedFacetInfo());
		target.setSizeFacetInfo(source.getSizeFacetInfo());
		target.setSexFacetInfo(source.getSexFacetInfo());
		target.setColourFacetInfo(source.getColourFacetInfo());
		target.setAlbumYearFacetInfo(source.getAlbumYearFacetInfo());
		target.setMovieYearFacetInfo(source.getMovieYearFacetInfo());
		target.setAlbumGenreFacetInfo(source.getAlbumGenreFacetInfo());
		target.setMovieGenreFacetInfo(source.getMovieGenreFacetInfo());
		target.setFormatFacetInfo(source.getFormatFacetInfo());

		return target;
	}

	private ArticlesSearchDaoInDto buildDaoInDto(ArticlesSearchServiceInDto serviceInDto) {
		ArticlesSearchDaoInDto daoInDto = new ArticlesSearchDaoInDto();
		daoInDto.setSearch(serviceInDto.getSearch());
		daoInDto.setFilters(buildDaoFilters(serviceInDto.getFilters()));
		daoInDto.setOrderBy(serviceInDto.getOrderBy());
		daoInDto.setFrom(serviceInDto.getFrom());
		if ("ASC".equals(serviceInDto.getOrderType().name())) {
			daoInDto.setOrderType(SortOrder.ASC);
		} else {
			daoInDto.setOrderType(SortOrder.DESC);
		}

		return daoInDto;
	}

	/**
	 * @param filtersServiceDto
	 * @return
	 */
	private FiltersDaoDto buildDaoFilters(FiltersServiceDto filtersServiceDto) {
		FiltersDaoDto daoFilters = new FiltersDaoDto();
		buildCategoryFilter(filtersServiceDto, daoFilters);
		buildAmountFilter(filtersServiceDto, daoFilters);
		buildDateFilter(filtersServiceDto, daoFilters);
		buildPopularFilter(filtersServiceDto, daoFilters);
		buildRecentlyAddedFilter(filtersServiceDto, daoFilters);
		buildSexFilter(filtersServiceDto, daoFilters);
		buildSizeFilter(filtersServiceDto, daoFilters);
		buildFormatFilter(filtersServiceDto, daoFilters);
		buildColourFilter(filtersServiceDto, daoFilters);
		buildAlbumGenreFilter(filtersServiceDto, daoFilters);
		buildAlbumYearFilter(filtersServiceDto, daoFilters);
		buildMovieGenreFilter(filtersServiceDto, daoFilters);
		buildMovieYearFilter(filtersServiceDto, daoFilters);
		buildBestSoldFilter(filtersServiceDto, daoFilters);
		buildDiscountFilter(filtersServiceDto, daoFilters);

		return daoFilters;
	}

	private void buildDiscountFilter(FiltersServiceDto filtersServiceDto, FiltersDaoDto daoFilters) {
		if (filtersServiceDto != null && filtersServiceDto.getDiscountFilter() != null) {
			daoFilters.setDiscountFilter(filtersServiceDto.getDiscountFilter());
		}
	}

	private void buildBestSoldFilter(FiltersServiceDto filtersServiceDto, FiltersDaoDto daoFilters) {
		if (filtersServiceDto != null && filtersServiceDto.getBestSoldFilter() != null) {
			daoFilters.setBestSoldFilter(filtersServiceDto.getBestSoldFilter());
		}
	}

	private void buildMovieYearFilter(FiltersServiceDto filtersServiceDto, FiltersDaoDto daoFilters) {
		if (filtersServiceDto != null && filtersServiceDto.getMovieYearFilter() != null) {
			daoFilters.setMovieYearFilter(filtersServiceDto.getMovieYearFilter());
		}
	}

	private void buildMovieGenreFilter(FiltersServiceDto filtersServiceDto, FiltersDaoDto daoFilters) {
		if (filtersServiceDto != null && filtersServiceDto.getMovieGenreFilter() != null) {
			daoFilters.setMovieGenreFilter(filtersServiceDto.getMovieGenreFilter());
		}
	}

	private void buildAlbumYearFilter(FiltersServiceDto filtersServiceDto, FiltersDaoDto daoFilters) {
		if (filtersServiceDto != null && filtersServiceDto.getAlbumYearFilter() != null) {
			daoFilters.setAlbumYearFilter(filtersServiceDto.getAlbumYearFilter());
		}

	}

	private void buildAlbumGenreFilter(FiltersServiceDto filtersServiceDto, FiltersDaoDto daoFilters) {
		if (filtersServiceDto != null && filtersServiceDto.getAlbumGenreFilter() != null) {
			daoFilters.setAlbumGenreFilter(filtersServiceDto.getAlbumGenreFilter());
		}

	}

	private void buildColourFilter(FiltersServiceDto filtersServiceDto, FiltersDaoDto daoFilters) {
		if (filtersServiceDto != null && filtersServiceDto.getColourFilter() != null) {
			daoFilters.setColourFilter(filtersServiceDto.getColourFilter());
		}

	}

	private void buildFormatFilter(FiltersServiceDto filtersServiceDto, FiltersDaoDto daoFilters) {
		if (filtersServiceDto != null && filtersServiceDto.getFormatFilter() != null) {
			daoFilters.setFormatFilter(filtersServiceDto.getFormatFilter());
		}

	}

	private void buildSizeFilter(FiltersServiceDto filtersServiceDto, FiltersDaoDto daoFilters) {
		if (filtersServiceDto != null && filtersServiceDto.getSizeFilter() != null) {
			daoFilters.setSizeFilter(filtersServiceDto.getSizeFilter());
		}
	}

	private void buildSexFilter(FiltersServiceDto filtersServiceDto, FiltersDaoDto daoFilters) {
		if (filtersServiceDto != null && filtersServiceDto.getSexFilter() != null) {
			daoFilters.setSexFilter(filtersServiceDto.getSexFilter());
		}
	}

	/**
	 * @param filtersServiceDto
	 * @param daoFilters
	 */
	private void buildRecentlyAddedFilter(FiltersServiceDto filtersServiceDto, FiltersDaoDto daoFilters) {
		if (filtersServiceDto != null && filtersServiceDto.getRecentlyAddedFilter() != null) {
			daoFilters.setRecentlyAddedFilter(filtersServiceDto.getRecentlyAddedFilter());
		}

	}

	/**
	 * @param filtersServiceDto
	 * @param daoFilters
	 */
	private void buildPopularFilter(FiltersServiceDto filtersServiceDto, FiltersDaoDto daoFilters) {
		if (filtersServiceDto != null && filtersServiceDto.getPopularFilter() != null) {
			daoFilters.setPopularFilter(filtersServiceDto.getPopularFilter());
		}

	}

	/**
	 * @param filtersServiceDto
	 * @param daoFilters
	 */
	private void buildDateFilter(FiltersServiceDto filtersServiceDto, FiltersDaoDto daoFilters) {
		if (filtersServiceDto != null && filtersServiceDto.getDateFilter() != null) {
			daoFilters.setDateFilter(filtersServiceDto.getDateFilter());
		}
	}

	/**
	 * @param filtersServiceDto
	 * @param daoFilters
	 */
	private void buildAmountFilter(FiltersServiceDto filtersServiceDto, FiltersDaoDto daoFilters) {
		if (filtersServiceDto != null && filtersServiceDto.getAmountFilter() != null) {
			daoFilters.setAmountFilter(filtersServiceDto.getAmountFilter());

		}
	}

	/**
	 * @param filtersServiceDto
	 * @return
	 */
	private void buildCategoryFilter(FiltersServiceDto filtersServiceDto, FiltersDaoDto daoFilters) {
		if (filtersServiceDto != null && filtersServiceDto.getCategoryFilter() != null) {
			daoFilters.setCategoryFilter(filtersServiceDto.getCategoryFilter());

		} else {
			daoFilters.setCategoryFilter(configurator.getCategories());
		}
	}

}
