package org.eac.amazing.services;

import org.eac.amazing.dtos.ArticlesSearchServiceInDto;
import org.eac.amazing.dtos.ArticlesSearchServiceOutDto;

public interface ArticlesService {

	public ArticlesSearchServiceOutDto search(ArticlesSearchServiceInDto inDto);
}
