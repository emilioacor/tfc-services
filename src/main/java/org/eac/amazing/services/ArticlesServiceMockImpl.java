package org.eac.amazing.services;

import java.util.ArrayList;
import java.util.List;

import org.eac.amazing.dtos.Article;
import org.eac.amazing.dtos.ArticlesSearchServiceInDto;
import org.eac.amazing.dtos.ArticlesSearchServiceOutDto;

//@Service
public class ArticlesServiceMockImpl implements ArticlesService {

	@Override
	public ArticlesSearchServiceOutDto search(ArticlesSearchServiceInDto inDto) {
		ArticlesSearchServiceOutDto outDto = new ArticlesSearchServiceOutDto();
		List<Article> articles = new ArrayList<Article>();
		Article article = new Article();
		article.setAmount(100.0);
		article.setArticleCode("12345");
		article.setArticleDescription("Videoconsola de �ltima generaci�n");
		article.setCategoryDescription("electronica");
		article.setName("PlayStation 4");
		article.setTradeMark("Sony");
		article.setCategoryCode("00001");
		articles.add(article);
		outDto.setArticles(articles);

		return outDto;
	}
}
