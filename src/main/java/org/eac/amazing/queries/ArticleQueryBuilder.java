/**
 * 
 */
package org.eac.amazing.queries;

import org.eac.amazing.core.query.SearchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */
public interface ArticleQueryBuilder {

	public QueryBuilder buildQuery(String search);

	public SearchQueryBuilder buildSearchQuery(QueryBuilder filteredQuery);
}
