/**
 * 
 */
package org.eac.amazing.queries;

import org.apache.commons.lang3.StringUtils;
import org.eac.amazing.core.commons.configuration.Configurator;
import org.eac.amazing.core.query.SearchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */
@Service
public class ArticleQueryBuilderImpl implements ArticleQueryBuilder {

	@Autowired
	private Configurator configurator;

	/**
	 * @param search
	 * @return
	 */
	public QueryBuilder buildQuery(String search) {
		QueryBuilder query;

		if (StringUtils.isEmpty(search)) {
			query = QueryBuilders.matchAllQuery();
		} else {
			query = QueryBuilders.multiMatchQuery(search, configurator.getMultiMatchFields());
		}
		return query;
	}

	/**
	 * @param filteredQuery
	 * @param article
	 * @return
	 */
	public SearchQueryBuilder buildSearchQuery(QueryBuilder filteredQuery) {
		SearchQueryBuilder searchQuery = new SearchQueryBuilder(filteredQuery);
		searchQuery.setIndices(configurator.getIndexes());
		searchQuery.setTypes(configurator.getTypes());

		return searchQuery;
	}

}
