package org.eac.amazing.daos;

import org.eac.amazing.dtos.ArticlesSearchDaoInDto;
import org.eac.amazing.dtos.ArticlesSearchDaoOutDto;

public interface ArticlesDao {

	ArticlesSearchDaoOutDto search(ArticlesSearchDaoInDto inDto);

}
