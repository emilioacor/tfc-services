package org.eac.amazing.daos;

import io.searchbox.core.Search;

import org.eac.amazing.core.commons.configuration.Configurator;
import org.eac.amazing.core.query.SearchQueryBuilder;
import org.eac.amazing.core.query.executor.QueryExecutor;
import org.eac.amazing.core.query.executor.QueryResult;
import org.eac.amazing.dtos.Article;
import org.eac.amazing.dtos.ArticlesSearchDaoInDto;
import org.eac.amazing.dtos.ArticlesSearchDaoOutDto;
import org.eac.amazing.facets.ArticleFacetBuilder;
import org.eac.amazing.filters.ArticleFilterBuilder;
import org.eac.amazing.queries.ArticleQueryBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */
@Service
public class ArticlesDaoImpl implements ArticlesDao {

	@Autowired
	private QueryExecutor queryExecutor;

	@Autowired
	private Configurator configurator;

	@Autowired
	private ArticleFacetBuilder articleFacetBuilder;

	@Autowired
	private ArticleFilterBuilder articleFilterBuilder;

	@Autowired
	private ArticleQueryBuilder articleQueryBuilder;

	@Override
	public ArticlesSearchDaoOutDto search(ArticlesSearchDaoInDto inDto) {

		final SearchQueryBuilder searchQueryBuilder = buildSearchQuery(inDto);

		QueryResult<Article> queryResult = queryExecutor.executeQueryWithResultAsList(new Search.Builder(
				searchQueryBuilder.toString()).addIndex(configurator.getIndexes()).addType(configurator.getTypes())
				.build(), Article.class);

		return processResult(queryResult);
	}

	/**
	 * @param queryResult
	 * @return
	 */
	private ArticlesSearchDaoOutDto processResult(QueryResult<Article> queryResult) {
		ArticlesSearchDaoOutDto outDto = new ArticlesSearchDaoOutDto();
		outDto.setArticles(queryResult.getResults());
		outDto.setFacetedInfoDto(articleFacetBuilder.buildFacetedInfo(queryResult.getRangeFacets(),
				queryResult.getTermsFacets()));
		outDto.setTotal(queryResult.getTotalElements());
		return outDto;
	}

	private SearchQueryBuilder buildSearchQuery(ArticlesSearchDaoInDto inDto) {

		QueryBuilder queryBuilder = articleQueryBuilder.buildQuery(inDto.getSearch());
		FilterBuilder filterBuilder = articleFilterBuilder.buildFilters(inDto.getFilters());

		SearchQueryBuilder searchQueryBuilder = articleQueryBuilder
				.buildSearchQuery(QueryBuilders.filteredQuery(queryBuilder, filterBuilder))
				.from(inDto.getFrom())
				.withSize(configurator.getSize())
				.sortedBy(inDto.getOrderBy().getField(), inDto.getOrderType())
				.withFacets(articleFacetBuilder.buildAmountFacet(), articleFacetBuilder.buildCategoryFacet(),
						articleFacetBuilder.buildSizeFacet(), articleFacetBuilder.buildPopularFacet(),
						articleFacetBuilder.buildRecentlyFacet(), articleFacetBuilder.buildSexFacet(),
						articleFacetBuilder.buildColourFacet(), articleFacetBuilder.builderAlbumGenreFacet(),
						articleFacetBuilder.builderMovieGenreFacet(), articleFacetBuilder.buildFormatFacet(),
						articleFacetBuilder.buildAlbumYearFacet(), articleFacetBuilder.buildMovieYearFacet(),
						articleFacetBuilder.buildDiscountFacet(), articleFacetBuilder.buildBestSoldFacet());

		return searchQueryBuilder;
	}

}
