package org.eac.amazing.daos;

import org.eac.amazing.core.commons.configuration.Configurator;
import org.eac.amazing.core.query.SearchQueryBuilder;
import org.eac.amazing.core.query.executor.QueryExecutor;
import org.eac.amazing.core.query.executor.QueryResult;
import org.eac.amazing.dtos.Article;
import org.eac.amazing.dtos.ArticlesSearchDaoInDto;
import org.eac.amazing.dtos.ArticlesSearchDaoOutDto;
import org.eac.amazing.facets.ArticleFacetBuilder;
import org.eac.amazing.filters.ArticleFilterBuilder;
import org.eac.amazing.queries.ArticleQueryBuilder;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilteredQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.facet.FacetBuilder;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Emilio Ambrosio Cordero
 * 
 */

public class ElasticArticlesDaoImpl implements ArticlesDao {

	@Autowired
	private QueryExecutor queryExecutor;

	@Autowired
	private Configurator configurator;

	@Autowired
	private ArticleFacetBuilder articleFacetBuilder;

	@Autowired
	private ArticleFilterBuilder articleFilterBuilder;

	@Autowired
	private ArticleQueryBuilder articleQueryBuilder;

	@Override
	public ArticlesSearchDaoOutDto search(ArticlesSearchDaoInDto inDto) {

		QueryBuilder queryBuilder = articleQueryBuilder.buildQuery(inDto.getSearch());
		FilterBuilder filterBuilder = articleFilterBuilder.buildFilters(inDto.getFilters());
		FacetBuilder amountFacet = articleFacetBuilder.buildAmountFacet();
		FacetBuilder categoryFacet = articleFacetBuilder.buildCategoryFacet();

		FilteredQueryBuilder filteredQueryBuilder = QueryBuilders.filteredQuery(queryBuilder, filterBuilder);

		QueryResult<Article> queryResult = queryExecutor.executeQueryWithResultAsList(filteredQueryBuilder,
				filterBuilder, new FacetBuilder[] { amountFacet, categoryFacet }, configurator.getIndexes(),
				configurator.getTypes(), 0, 10, Article.class);

		return processResult(queryResult);
	}

	/**
	 * @param queryResult
	 * @return
	 */
	private ArticlesSearchDaoOutDto processResult(QueryResult<Article> queryResult) {
		ArticlesSearchDaoOutDto outDto = new ArticlesSearchDaoOutDto();
		outDto.setArticles(queryResult.getResults());
		outDto.setFacetedInfoDto(articleFacetBuilder.buildFacetedInfo(queryResult.getRangeFacets(),
				queryResult.getTermsFacets()));
		return outDto;
	}

	private SearchQueryBuilder buildSearchQuery(ArticlesSearchDaoInDto inDto, Integer from, Integer size) {

		QueryBuilder queryBuilder = articleQueryBuilder.buildQuery(inDto.getSearch());
		FilterBuilder filterBuilder = articleFilterBuilder.buildFilters(inDto.getFilters());
		FacetBuilder amountFacet = articleFacetBuilder.buildAmountFacet();

		FacetBuilder categoryFacet = articleFacetBuilder.buildCategoryFacet();
		SearchQueryBuilder searchQueryBuilder = articleQueryBuilder
				.buildSearchQuery(QueryBuilders.filteredQuery(queryBuilder, filterBuilder)).from(from).withSize(size)
				.withFacets(amountFacet, categoryFacet);

		return searchQueryBuilder;
	}

}
