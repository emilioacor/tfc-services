#!/bin/sh

#
#  Script para creación del índice index.
#
#  Novedades en esta versión: Se elimina el tipo notasYfavorito, y pasa su contenido
#  como un array dentro del tipo movimientos.
#

curl -XDELETE 'http://localhost:9200/index'
echo 
curl -XPOST 'http://localhost:9200/index?pretty' -d '{
  "settings" : {
      "number_of_replicas" : 2,
      "number_of_shards" : 15,
      "action.auto_create_index": false,
		
      "analysis" : {
          "analyzer" : {
	      "custom_analyzer" : {
	          "tokenizer" : "standard",
	          "filter" : [ "lowercase", "asciifolding" ]
	      }
          }
      }
  },


  "mappings" : {


   
		"articles" : {
		
			"dynamic" : "strict",
			"_routing": { "required": false, "path": "categoryCode" },		

			"_all": {
				"enabled": false
			},
		
			"properties" : {
				
				"articleCode" : {
					"type" : "string",
					"index" : "not_analyzed"
				},

				"categoryCode" : {
					"type" : "string",
					"index" : "not_analyzed"
				},

				"name" : {
					"type" : "string",
					"analyzer" : "custom_analyzer"
				},

				"amount" : {
					"type" : "long"
					
				},

				"categoryDescription" : {
					"type" : "string",
					"analyzer" : "custom_analyzer"
				},

				"articleDescription" : {
					"type" : "string",
					"analyzer" : "custom_analyzer"
				},

				"tradeMark" : {
					"type" : "string",
					"analyzer" : "custom_analyzer"
				},

				"recentlyAdded" : {
					"type" : "boolean"
				},

				"popular" : {
					"type" : "boolean"
				},

				"size" : {
					"type" : "string",
					"analyzer" : "custom_analyzer"
				},

				"sex" : {
					"type" : "string",
					"analyzer" : "custom_analyzer"
				},

				"colour" : {
					"type" : "string",
					"analyzer" : "custom_analyzer"
				},

				"album" : {
					"type" : "string",
					"analyzer" : "custom_analyzer"
				},

				"albumGenre" : {
					"type" : "string",
					"analyzer" : "custom_analyzer"
				},

				"albumYear" : {
					"type" : "integer"
				},

				"author" : {
					"type" : "string",
					"analyzer" : "custom_analyzer"
				},

				"director" : {
					"type" : "string",
					"analyzer" : "custom_analyzer"
				},

				"movieYear" : {
					"type" : "integer"
				},

				"movieGenre" : {
					"type" : "string",
					"analyzer" : "custom_analyzer"
				},

				"format" : {
					"type" : "string",
					"analyzer" : "custom_analyzer"
				}
			}
		}
	}
}'
echo




