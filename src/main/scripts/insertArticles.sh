#!/bin/bash

curl -XPOST 'http://localhost:9200/_bulk?pretty' --data-binary @bulkInsertArticles_category01.json
curl -XPOST 'http://localhost:9200/_bulk?pretty' --data-binary @bulkInsertArticles_category02.json
curl -XPOST 'http://localhost:9200/_bulk?pretty' --data-binary @bulkInsertArticles_category03.json
curl -XPOST 'http://localhost:9200/_bulk?pretty' --data-binary @bulkInsertArticles_category04.json
curl -XPOST 'http://localhost:9200/_bulk?pretty' --data-binary @bulkInsertArticles_category05.json
#curl -XPOST 'http://localhost:9200/_bulk?pretty' --data-binary @bulkInsertArticles_category06.json