package org.eac.amazing.remoting;

import java.util.ArrayList;

import org.eac.amazing.dtos.Article;
import org.eac.amazing.dtos.ArticlesSearchInDto;
import org.eac.amazing.dtos.ArticlesSearchOutDto;
import org.eac.amazing.dtos.ArticlesSearchServiceInDto;
import org.eac.amazing.dtos.ArticlesSearchServiceOutDto;
import org.eac.amazing.dtos.FacetedInfoServiceDto;
import org.eac.amazing.dtos.FiltersDto;
import org.eac.amazing.filters.GenericRangeFilter;
import org.eac.amazing.services.ArticlesService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ArticlesOperationsImplTest {

	@InjectMocks
	private ArticlesOperationsImpl serviceImpl = new ArticlesOperationsImpl();

	private ArticlesOperations service;

	@Mock
	private ArticlesService serviceMock;

	private ArticlesSearchInDto inDto;

	private ArticlesSearchServiceOutDto outServiceDto;

	@Before
	public void setup() {
		configureInDto();
		configureServiceMock();
	}

	private void configureServiceMock() {
		Mockito.reset(serviceMock);
		service = serviceImpl;
		outServiceDto = new ArticlesSearchServiceOutDto();
		outServiceDto.setArticles(new ArrayList<Article>());
		outServiceDto.setFacetedInfoDto(new FacetedInfoServiceDto());
		Mockito.doReturn(outServiceDto).when(serviceMock).search(Matchers.any(ArticlesSearchServiceInDto.class));

	}

	private void configureInDto() {
		inDto = new ArticlesSearchInDto();
		inDto.setSearch("");
		FiltersDto filters = new FiltersDto();
		filters.setAmountFilter(new GenericRangeFilter<Long>());
		inDto.setFilters(filters);
	}

	@Test
	public void testSearch() {
		ArticlesSearchOutDto outDto = service.search(inDto);

		Assert.assertNotNull(outDto);
	}
}
