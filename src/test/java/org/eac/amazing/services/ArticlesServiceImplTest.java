package org.eac.amazing.services;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.eac.amazing.core.commons.configuration.Configurator;
import org.eac.amazing.core.commons.enums.OrderFieldEnum;
import org.eac.amazing.core.commons.enums.OrderTypeEnum;
import org.eac.amazing.daos.ArticlesDao;
import org.eac.amazing.dtos.Article;
import org.eac.amazing.dtos.ArticlesSearchDaoInDto;
import org.eac.amazing.dtos.ArticlesSearchDaoOutDto;
import org.eac.amazing.dtos.ArticlesSearchServiceInDto;
import org.eac.amazing.dtos.ArticlesSearchServiceOutDto;
import org.eac.amazing.dtos.FacetedInfoDaoDto;
import org.eac.amazing.dtos.GenericRangeFacetInfo;
import org.eac.amazing.dtos.TermFacetInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ArticlesServiceImplTest {

	@InjectMocks
	private ArticlesServiceImpl serviceImpl = new ArticlesServiceImpl();

	@Mock
	private ArticlesDao daoMock;

	@Mock
	private Configurator configurator;

	private ArticlesService service;

	@Before
	public void setup() {
		configureMock();
	}

	private void configureMock() {

		Mockito.reset(daoMock);
		service = serviceImpl;
		ArticlesSearchDaoOutDto outDto = buildDaoOutDto();
		Mockito.doReturn(outDto).when(daoMock).search(Matchers.any(ArticlesSearchDaoInDto.class));
	}

	private ArticlesSearchDaoOutDto buildDaoOutDto() {
		ArticlesSearchDaoOutDto daoOutDto = new ArticlesSearchDaoOutDto();
		daoOutDto.setArticles(buildArticles());
		daoOutDto.setFacetedInfoDto(buildFacetedInfoDto());
		daoOutDto.setTotal(6);

		return daoOutDto;
	}

	private FacetedInfoDaoDto buildFacetedInfoDto() {
		FacetedInfoDaoDto facetedInfoDto = null;
		facetedInfoDto = new FacetedInfoDaoDto();
		facetedInfoDto.setAlbumGenreFacetInfo(new TermFacetInfo("albumGenre", null));
		facetedInfoDto.setAlbumYearFacetInfo(new TermFacetInfo("albumYear", null));
		facetedInfoDto.setAmountFacetInfo(new GenericRangeFacetInfo<Double>("amount", null));
		facetedInfoDto.setBestSoldFacetInfo(new TermFacetInfo("bestSold", null));
		facetedInfoDto.setCategoryFacetInfo(new TermFacetInfo("category", null));
		facetedInfoDto.setColourFacetInfo(new TermFacetInfo("colour", null));
		facetedInfoDto.setDiscountFacetInfo(new TermFacetInfo("discount", null));
		facetedInfoDto.setFormatFacetInfo(new TermFacetInfo("format", null));
		facetedInfoDto.setMovieGenreFacetInfo(new TermFacetInfo("movieGenre", null));
		facetedInfoDto.setMovieYearFacetInfo(new TermFacetInfo("movieYear", null));
		facetedInfoDto.setPopularFacetInfo(new TermFacetInfo("popular", null));
		facetedInfoDto.setRecentlyAddedFacetInfo(new TermFacetInfo("recentlyAdded", null));
		facetedInfoDto.setSexFacetInfo(new TermFacetInfo("sex", null));
		facetedInfoDto.setSizeFacetInfo(new TermFacetInfo("size", null));

		return facetedInfoDto;
	}

	private List<Article> buildArticles() {
		List<Article> articles = new ArrayList<Article>();
		articles.add(new Article("phone", 100.10, "00001", "category 00001", "0000100001", "description", "tradeMark",
				true, true, false, false, null, null, null, null, null, null, null, null, null, null, null, null));
		articles.add(new Article("ebook", 250.50, "00002", "category 00002", "0000200001", "description", "tradeMark",
				false, true, false, false, null, null, null, null, null, null, null, null, null, null, null, null));
		articles.add(new Article("video game", 45.99, "00003", "category 00003", "0000300001", "description",
				"tradeMark", true, true, false, false, null, null, null, null, null, null, null, null, null, null,
				null, null));
		articles.add(new Article("t-shirt", 20.0, "00004", "category 00004", "0000400001", "description", "tradeMark",
				true, true, false, false, null, null, null, null, null, "red", "XL", "male", null, null, null, null));
		articles.add(new Article("best of rock", 35.10, "00005", "category 00005", "0000500001", "description",
				"tradeMark", true, true, false, false, null, null, null, null, null, null, null, null, "best of...",
				"VVAA", "rock", "2014"));
		articles.add(new Article("Avengers", 50.10, "00006", "category 00006", "0000600001", "description",
				"tradeMark", true, true, false, false, null, "director", "action", 2014, "Blue-Ray", null, null, null,
				null, null, null, null));

		return articles;
	}

	@Test
	public void searchWithEmptyText() {
		ArticlesSearchServiceInDto serviceInDto = new ArticlesSearchServiceInDto();
		serviceInDto.setSearch("");
		serviceInDto.setFilters(null);
		serviceInDto.setFrom(0);
		serviceInDto.setOrderBy(OrderFieldEnum.POPULAR);
		serviceInDto.setOrderType(OrderTypeEnum.ASC);

		ArticlesSearchServiceOutDto serviceOutDto = service.search(serviceInDto);

		Assert.assertEquals(6, serviceOutDto.getArticles().size());

	}
}
