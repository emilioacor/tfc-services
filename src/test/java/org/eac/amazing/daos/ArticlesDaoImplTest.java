package org.eac.amazing.daos;

import org.eac.amazing.core.commons.configuration.Configurator;
import org.eac.amazing.core.query.executor.QueryExecutor;
import org.eac.amazing.facets.ArticleFacetBuilder;
import org.eac.amazing.filters.ArticleFilterBuilder;
import org.eac.amazing.queries.ArticleQueryBuilder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ArticlesDaoImplTest {

	@InjectMocks
	private ArticlesDao dao = new ArticlesDaoImpl();
	@Mock
	private QueryExecutor queryExecutor;

	@Mock
	private Configurator configurator;

	@Mock
	private ArticleFacetBuilder articleFacetBuilder;

	@Mock
	private ArticleFilterBuilder articleFilterBuilder;

	@Mock
	private ArticleQueryBuilder articleQueryBuilder;

}
